# slewsystems.com
Company website built with Gatsby.

## Development
1. Install dependencies
    ```bash
    yarn install
    ```

1. Start local server
    ```bash
    yarn dev:start
    ```

## Deployment
1. Install dependencies
    ```bash
    yarn install
    ```

1. Build static site
    ```bash
    yarn build
    ```