const path = require('path');

module.exports = {
  siteMetadata: {
    title: `Slew Systems LLC`,
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-typography',
      options: {
        pathToConfigModule: 'src/utils/typography'
      },
    },
    { 
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: path.join(__dirname, 'src', 'images'),
      },
    },
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'Slew Systems',
        background_color: '#d7fbff',
        theme_color: '#d7fbff',
        icon: 'src/images/symbol.png',
      },
    },
    'gatsby-plugin-sass',
    'gatsby-plugin-typescript',
    'gatsby-plugin-react-helmet',
    'gatsby-transformer-sharp', 
    'gatsby-plugin-sharp',
  ],
}