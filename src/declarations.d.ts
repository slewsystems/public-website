// This file is used to hold ambient type declarations, as well as type shims
// for npm module without type declarations, and assets files.

declare module '*.svg';
